<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title>Proprietary a Real Estates and Builders </title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Proprietary a Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>	
<!-- font files -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css">
<link href='//fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Poiret+One' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<!-- /font files -->
<!-- css files -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/update.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/photoGallery.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/progressbar.css" rel="stylesheet" type="text/css" media="all"/>
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />



<!-- /css files -->
<!-- js files -->

<!-- /js files -->	
<style>
	.txt
	{
		background:gray;
		color:white;
		font-weight:bold;
		margin-bottom:10px;
	}
	.lconbox
	{
		position:fixed;
		top:20vh;
		right:40px;
		background:rgba(0,153,255,0.9);
		min-height:300px;
		width:400px;
		padding:20px;
		border-top:5px solid white;
	}
	.enq-box
	{
		position:fixed;
		bottom:0;
		left:0;
		width:100%;
		min-height:20px;
		z-index:9;
		display:none;
	}
	.enq-box .container
	{
		background:rgba(0,153,255,0.9);
		padding:15px 25px;
	}
	.enq-after-close
	{
		position:fixed;
		right:0;
		bottom:15vh;
		background:rgba(0,153,255,0.9);
		padding:5px 10px;
		height:auto;
		display:none;
		cursor:pointer;
		z-index:8;
	}
	.cd-top
	{
		z-index:10;
	}
</style>
<script>]

	$(document).ready(function(){
		
		
	});
</script>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
<!-- navigation -->
<div class="navbar-wrapper">
    <div class="container">
		<nav class="navbar navbar-inverse navbar-static-top cl-effect-21">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="index.php"><h1><span>A</span>MBIENCE CREACIONS</h1></a>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="index.php">Home</a></li>
						<li><a href="#about">About</a></li>
				<!--			<li><a href="#service">Services</a></li>	-->	
				<!--			<li><a href="#skill">Our Skills</a></li>	-->	
						<li><a href="#gallery">Gallery</a></li>
			<!--			<li><a href="#contact">Contact</a></li>		-->
					</ul>
				</div>
             
             
             <div class="lconbox">
                <form method="post" action="">
                <h3 style="text-align:center;color:white;">CONTACT US</h3><hr>
                       <input type="text" placeholder="Name*" class="form-control txt" id="exampleFormControlInput1" name="name" required>
                       
                       <input type="text" placeholder="Email*" class="form-control txt" id="exampleFormControlInput1" name="email" required>
                       
                       <input type="text" placeholder="Mobile*" class="form-control txt" id="exampleFormControlInput1" name="mobile" required>
                       
                      <br>
                       <button class="btn btn-outline1 btn-lg" type="submit" name="sub1" onClick="time()" >Send</button>
                       
                       <br><br>
                       <label class="exampleInputEmail1" style="color:white;">*Provided information is confidential and it will not be used for any promotional activity.</label>
	                   <br><center><a href="tel:+91-8010531053" style="text-decoration:none;color:white;font-weight:bold;">+91-8010531053</a></center>
                       </form>
             </div>
             
             
            <div id="demo" class="carousel slide" data-ride="carousel" data-interval="2000">
                <!--<div id="demo" class="carousel slide" data-ride="carousel" data-interval="3000"></div> -->
                <!-- Indicators -->
            </div>
            
        </nav>
    </div>
</div>
<!-- /navigation -->
<!-- banner section -->
<div class='header'>
</div>
<!-- /banner section -->
<div class="enq-after-close">
	<span style="font-size:20px;color:white;">Enquiry</span>
</div>
<footer class="enq-box">
	<div class="container">
	<form  method="post" action="" >
	<a class="close" id="enqclose" style="font-size:60px;user-select:none;margin-top:-22px;">&times;</a>
    <h4 style="text-align:center;color:white;padding-bottom:15px;">CONTACT US</h4>
    	<div class="col-sm-1"></div>
    	<div class="col-sm-3"><input type="text" placeholder="Name*" class="form-control txt" id="exampleFormControlInput1" name="name" required></div>
        <div class="col-sm-3"><input type="text" placeholder="Email*" class="form-control txt" id="exampleFormControlInput1" name="email" required></div>
        <div class="col-sm-3"><input type="text" placeholder="Mobile*" class="form-control txt" id="exampleFormControlInput1" name="mobile" required></div>
        <div class="col-sm-2"><button class="btn btn-outline1 btn-lg"  type="submit"  name="sub2" onClick="time()">Send</button></div>
       
        <center><label class="exampleInputEmail1" style="color:white;">*Provided information is confidential and it will not be used for any promotional activity.</label>
	                   <br><a href="tel:+91-8010531053" style="text-decoration:none;color:white;font-weight:bold;">+91-8010531053</a></center>
	</form>    
    </div>
</footer>
<script>
var ct=1;
$(".subbtn").click(function(){
		ct++;
	});
	if(ct!=1)
	{
		window.location="thankyou.php";
	}
	var a=1;
	$(".enq-after-close").click(function(){
			$(".enq-box").fadeIn();
			$(".enq-after-close").fadeOut();
			a--;
		});
		$("#enqclose").click(function(){
			$(".enq-box").fadeOut();
			$(".enq-after-close").fadeIn();
			a++;
		});
$(window).scroll(function(){
	var wid=$(window).width();
	if(wid>=728 && a==1)
	{
	var scr=$(window).scrollTop();
		if(scr>=20){
			$(".enq-box").fadeIn();
			$(".lconbox").fadeOut();
		}
		else if(scr<20){
			$(".lconbox").fadeIn();
			$(".enq-box").fadeOut();
		}
	}
	else if(wid<728 && a==1){
		$(".lconbox").fadeOut();	
		$(".enq-box").fadeIn();
	}
			});

</script>



						<?php
include("admin/config.php");
if(isset($_POST["sub1"]))
{
	$name=$_POST["name"];
	$email=$_POST["email"];
	$mobile=$_POST["mobile"];	

	$ins="insert into sdm (name,email,mobile) values('$name','$email','$mobile')";
	$b=mysqli_query($link,$ins);
if($b)
	{
		
	echo '<script>alert("Your are already register"); location.href="index.php";</script>';
	}
	else
	{
		$msg="Invalid User Name or Password";
	}		
		}
?>
						<?php
include("admin/config.php");
if(isset($_POST["sub2"]))
{
	$name=$_POST["name"];
	$email=$_POST["email"];
	$mobile=$_POST["mobile"];	
	$descr=$_POST["descr"];
	$ins="insert into sdm (name,email,mobile) values('$name','$email','$mobile')";
	$b=mysqli_query($link,$ins);
	header("location:thankyou.php");

		}
?>

    <script type="text/javascript">
         <!--
            function Redirect() {
               window.location="thankyou.php";
			

			   
            }
         //-->
		 function time()
		 {
			    setTimeout("location.href = 'http://localhost/tsdm/admin/thankyou.php';", 1500);
			 			   setTimeout("location.href = 'http://localhost/tsdm/admin/thankyou.php';",1500);
			 } 

      </script>
      
    <!--  	   <meta http-equiv="refresh" content="2; url=http://localhost/tsdm/admin/thankyou.php" />  -->
    
    
    
