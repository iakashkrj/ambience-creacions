
<!-- news section -->
<section class="our-updates" id="news">
	<h3 class="text-center slideanim">Our Latest Updates</h3>
	<p class="text-center slideanim">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-6">
				<div class="row">
					<div class="col-xs-6 news-info">
						<div class="view view-eighth slideanim">
							<img src="images/update1.jpg" />
							<div class="mask">
								<h4>Dec-21-2016</h4>
								<p>Check Out Our Latest Updates.</p>
								<a href="#contact" class="info">Locate Us</a>
							</div>
						</div>
					</div>
					<div class="col-xs-6 news-info">
						<div class="update-details slideanim">
							<h4>This is the latest news update right here.</h4>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
							<a href="#contact">Contact Us</a>
						</div>
					</div>
				</div>
			</div>	
			<div class="col-lg-6 col-md-6">
				<div class="row">
					<div class="col-xs-6 news-info">
						<div class="view view-eighth slideanim">
							<img src="images/update2.jpg" />
							<div class="mask">
								<h4>Dec-22-2016</h4>
								<p>Check Out Our Latest Updates.</p>
								<a href="#contact" class="info">Locate Us</a>
							</div>
						</div>
					</div>
					<div class="col-xs-6 news-info">
						<div class="update-details slideanim">
							<h4>This is the latest news update right here.</h4>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
							<a href="#contact">Contact Us</a>
						</div>
					</div>
				</div>
			</div>
		</div>		
		<div class="row update">
			<div class="col-lg-6 col-md-6">
				<div class="row">
					<div class="col-xs-6 news-info">
						<div class="view view-eighth slideanim">
							<img src="images/update3.jpg" />
							<div class="mask">
								<h4>Dec-27-2016</h4>
								<p>Check Out Our Latest Updates.</p>
								<a href="#contact" class="info">Locate Us</a>
							</div>
						</div>
					</div>
					<div class="col-xs-6 news-info">
						<div class="update-details slideanim">
							<h4>This is the latest news update right here.</h4>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
							<a href="#contact">Contact Us</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6">
				<div class="row">
					<div class="col-xs-6 news-info">
						<div class="view view-eighth slideanim">
							<img src="images/update4.jpg" />
							<div class="mask">
								<h4>Dec-29-2016</h4>
								<p>Check Out Our Latest Updates.</p>
								<a href="#contact" class="info">Locate Us</a>
							</div>
						</div>
					</div>
					<div class="col-xs-6 news-info">
						<div class="update-details slideanim">
							<h4>This is the latest news update right here.</h4>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
							<a href="#contact">Contact Us</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>		
</section>
<!-- /news section -->