<!-- about section -->
<section class="about-us" id="about">
	<h2 class="text-center slideanim">About Us</h2>
    
    
    
    
	<p class="text-center slideanim"></p>
	
    <div class="container">
  <div class="jumbotron">
   <div align="center" style="color:#333"> <h1>AMBIENCE CREACIONS</h1> </div>
    <br>
    <br>
    <p>Ambience Creacions Gurgaon is a residential development that is elegantly crafted to offer the highest level of luxury, both inside and outside as well. The apartments are spacious, provide natural lighting, ventilation and contain branded fittings and fixtures, imported wood/Indian teak internal doors, UPVC/aluminium coated windows providing landscape views, large balconies and beautifully designed main doors, high-quality Spanish/Italian/Turkish flooring in living & dining room, imported Italian marble/wood flooring in bedrooms, fancy Italian modular kitchens with granite/marble counters, piped gas and multiple provisions, high-quality emulsion paint, electrical ports located perfectly with modular switches, video door phone and more. Experience luxury and comfort that will always always make you feel like ‘home’.</p> 
  </div>
 
</div>

    
    <font size="+1" >
  <div class="col-lg-12 slideanim" >
			
			<div align="center" style="color:#FFF"><h1>Location Advantage </h1></div>
			<div class="container-fluid">
			
			<br>
			<br>
			<br>
		<div class="row" style="color:#F00" style="font:bolder" style="font-size:500px">
		
		 <div class="col-sm-3 col-md-6" >
        <div class="panel-group" >
            <div class="panel panel-default">
                <div class="panel-body">Location : 22-Sector, Gurgaon</div>
            </div>
            <div class="panel panel-default">
                <div class="panel-body">7 Minutes From Revamped Iffco Chowk</div>
            </div>
        </div>
        <div class="panel-group">
            <div class="panel panel-default">
                <div class="panel-body">30 Minutes From NH-8</div>
            </div>
            <div class="panel panel-default">
                <div class="panel-body">28 Minutes From Airport</div>
            </div>
        </div>
        <div class="panel-group">
            <div class="panel panel-default">
                <div class="panel-body">Near From Famous Food Destinations</div>
            </div>
            <div class="panel panel-default">
                <div class="panel-body">Near From Major Shopping Destination</div>
            </div>
        </div>
        <div class="panel-group">
            <div class="panel panel-default">
                <div class="panel-body">Near From Major Schools / Hospitals / Hotels</div>
            </div>
           
        </div>
        </div>
          <div class="col-sm-3 col-md-6">
          <img src="images/img1.png" height="435" style="width:100%;"></div>
          <div>
    </div>
    
    <div > 
		<div class="row">
			<div class="col-lg-12">
				<img src="images/1.jpeg" height="525" style="width:100%;">
			</div>
		</div>
	</div>
</section>
<!-- /about section -->



