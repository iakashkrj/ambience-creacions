
<!-- services section -->
<section class="our-services slideanim" id="service">
	<h3 class="text-center slideanim">SALIENT FEATURES</h3>
	<p class="text-center slideanim"></p>
    <img src="images/c2.jpg" height="700" width="1350">
	<div class="container">
		<div class="row">
        
		<!--	<div class="col-lg-4 col-md-4 col-sm-6">
				<div class="serv-info">
					<i class="fa fa-trophy slideanim" aria-hidden="true"></i>
					<h4 class="text-center slideanim">Lorem Ipsum</h4>
					<p class="serv slideanim">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6">
				<div class="serv-info">
					<i class="fa fa-cubes slideanim" aria-hidden="true"></i>
					<h4 class="text-center slideanim">Lorem Ipsum</h4>
					<p class="serv slideanim">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 last-serv">
				<div class="serv-info">
					<i class="fa fa-life-ring slideanim" aria-hidden="true"></i>
					<h4 class="text-center slideanim">Lorem Ipsum</h4>
					<p class="serv slideanim">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
				</div>
			</div>
		</div>
	</div>
</section>  -->
<!-- /services section -->
<!-- skill section -->
<section class="our-skills" id="skill">
	<h3 class="text-center slideanim">Our Skills</h3>
	<p class="text-center slideanim">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
	<div class="container">	
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 slideanim">	
				<ul class='skills'>
					<li class='progressbarPhp' data-width='70' data-target='100'>Marketing 70%</li>
				</ul>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 slideanim">	
				<ul class='skills'>
					<li class='progressbarPhp' data-width='90' data-target='100'>Development 90%</li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 slideanim">	
				<ul class='skills'>
					<li class='progressbarPhp' data-width='65' data-target='100'>Business 65%</li>
				</ul>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 slideanim">	
				<ul class='skills'>
					<li class='progressbarPhp' data-width='80' data-target='100'>Trading 80%</li>
				</ul>
			</div>
		</div>
	</div>	
</section>
<!-- skill section -->