-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 07, 2018 at 06:31 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sdm`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`email`, `password`) VALUES
('admin@gmail.com', 'f865b53623b121fd34ee5426c792e5c33af8c227');

-- --------------------------------------------------------

--
-- Table structure for table `sdm`
--

CREATE TABLE `sdm` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `descr` varchar(10000) NOT NULL,
  `dat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sdm`
--

INSERT INTO `sdm` (`id`, `name`, `email`, `mobile`, `descr`, `dat`) VALUES
(2, 'AKASH KUMAR JAISWAL', 'akashkumarjaiswal9889@gmail.com', '9621176062', 'nds', '2018-09-07 14:43:42'),
(3, 'AKASH KUMAR JAISWAL', 'akashkumarjaiswal9889@gmail.com', '9621176062', 'nds', '2018-09-07 14:44:34'),
(4, 'AKASH KUMAR JAISWAL', 'akashkumarjaiswal9889@gmail.com', '9621176062', 'nds', '2018-09-07 14:45:17'),
(5, 'AKASH KUMAR JAISWAL', 'akashkumarjaiswal9889@gmail.com', '9621176062', 'nds', '2018-09-07 14:46:28'),
(6, 'AKASH KUMAR JAISWAL', 'akashkumarjaiswal9889@gmail.com', '9621176062', 'nds', '2018-09-07 14:48:19'),
(7, 'AKASH KUMAR JAISWAL', 'akashkumarjaiswal9889@gmail.com', '9621176062', 'nds', '2018-09-07 14:49:17'),
(8, 'AKASH KUMAR JAISWAL', 'akashkumarjaiswal9889@gmail.com', '9621176062', 'nds', '2018-09-07 14:53:38'),
(9, 'AKASH KUMAR JAISWAL', 'akashkumarjaiswal9889@gmail.com', '9621176062', 'nds', '2018-09-07 14:54:42'),
(10, 'AKASH KUMAR JAISWAL', 'akashkumarjaiswal9889@gmail.com', '9621176062', 'nds', '2018-09-07 14:56:02'),
(11, 'AKASH KUMAR JAISWAL', 'akashkumarjaiswal9889@gmail.com', '9621176062', 'nds', '2018-09-07 14:56:08'),
(12, 'AKASH KUMAR JAISWAL', 'akashkumarjaiswal9889@gmail.com', '9621176062', 'nds', '2018-09-07 14:56:17'),
(13, 'AKASH KUMAR JAISWAL', 'akashkumarjaiswal9889@gmail.com', '9621176062', 'nds', '2018-09-07 14:56:45'),
(14, 'AKASH KUMAR JAISWAL', 'akashkumarjaiswal9889@gmail.com', '9621176062', 'nds', '2018-09-07 16:27:35'),
(15, 'AKASH KUMAR JAISWAL', 'akashkumarjaiswal9889@gmail.com', '9621176062', 'nds', '2018-09-07 16:28:21'),
(16, 'AKASH KUMAR JAISWAL', 'akashkumarjaiswal9889@gmail.com', '9621176062', 'nds', '2018-09-07 16:30:28');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `sdm`
--
ALTER TABLE `sdm`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sdm`
--
ALTER TABLE `sdm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
